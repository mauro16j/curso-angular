  
import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  public seleccion: boolean;
  public servicios: string[];
  public id = uuid();
  public votes = 0;
  constructor(public nombre: string, public imagenUrl: string) {
       this.servicios = ['pileta', 'desayuno'];
  }
  setSelected(s: boolean) {
    this.seleccion = s;
  }
  isSelected() {
    return this.seleccion;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}