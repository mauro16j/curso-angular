import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destino-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

@Injectable()
export class DestinoApiClient {
    destinos: DestinoViaje[];
    //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
    constructor(private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient){
        //this.destinos = [];
        this.store
        .select(state => state.destinos)
        .subscribe((data)=> {
            console.log('destinos sub store');
            console.log(data);
            this.destinos = data.items;
        })
        
    }

    /*add(dest: DestinoViaje){
       // this.destinos.push(dest);
       this.store.dispatch(new NuevoDestinoAction(dest));
       
    }*/

    /*getAll():DestinoViaje[]{
        return this.destinos;
    }*/

    add(d: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
          if (data.status === 200) {
            this.store.dispatch(new NuevoDestinoAction(d));
            const myDb = db;
            myDb.destinos.add(d);
            console.log('todos los destinos de la db!');
            myDb.destinos.toArray().then(destinos => console.log(destinos))
          }
        });
      }
      
    getById(id: string):DestinoViaje{
        return this.destinos.filter(function(dest){
            return dest.id.toString() === id;
        })[0];
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    elegir(d:DestinoViaje) {
        /*this.destinos.forEach(x => x.setSelected(false));
        d.setSelected(true);
        this.current.next(d);*/
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    /*subscribeOnChange(fn){
        this.current.subscribe(fn);
    }*/

}