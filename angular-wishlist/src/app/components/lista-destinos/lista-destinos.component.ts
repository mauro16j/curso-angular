import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinoApiClient } from '../../models/destino-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinoApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  public updates: string[];
  all;

  
  constructor(private destinoApiclient: DestinoApiClient,
              private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    
   }

  ngOnInit() {
    this.store.select(state => state.destinos.favorito)
        .subscribe(d => {
          if(d != null) {
            this.updates.push('Se ha elegido a ' + d.nombre);
          }
        });
       this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
    /*this.destinoApiclient.subscribeOnChange((d: DestinoViaje) => {
      if(d ! = null) {
            this.updates.push('Se ha elegido a ' + d.nombre);
          }
    });*/
  }

  agregado(d: DestinoViaje) {
    this.destinoApiclient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(d: DestinoViaje){
    this.destinoApiclient.elegir(d);
    //this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getAll(){

  }

}
